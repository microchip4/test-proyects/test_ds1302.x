/*
 * File:   main_DS1302.c
 * Author: Asus
 *
 * Created on 2020/05/20, 20:46
 */

#define CLOCK_20MHZ
#include <xc.h>
#include <stdlib.h>
#include "Alteri.h"
#include "PCD8544.h"
#include "DS1302_RTC.h"

char Time[8];
char Hour[3];
char Minutes[3];
char Seconds[3];
char Day[3];
char var1[3];
char var2[3];
char var3[3];
char var4[3];
char var5[3];

void main(void) {
    ADCON0bits.ADON = 0;//0 = A/D converter module is disabled
    ADCON1bits.PCFG = 0b1111;   //All Digital
    TRISBbits.RB0 = 1;  //Serial data  (SDA) ? RB0/AN12/INT0/FLT0/SDI/SDA
    TRISBbits.RB1 = 1;  //Serial clock (SCL) ? RB1/AN10/INT1/SCK/SCL
    //OpenI2C(MASTER,SLEW_OFF);
    //SSPADD = 49;    // 100 kHz 100000=20000000/(4*(x+1))
    LCD_NOKIA_Init();
    LCD_NOKIA_Clear();
    DS1302_Init();
    //DS1302_setTIME(20,5,6,5,15,43,00);
    while(1){
        LCD_NOKIA_WriteString("Hello!1",0,0);
        DS1302_getTIME(Time);
        itoa(var5,Time[7],10);
        itoa(var4,Time[6],10);
        itoa(var3,Time[5],10);
        itoa(var2,Time[4],10);
        itoa(var1,Time[3],10);
        itoa(Hour,Time[2],10);
        itoa(Minutes,Time[1],10);
        itoa(Seconds,Time[0],10);
        LCD_NOKIA_WriteString(Hour,0,1);
        LCD_NOKIA_WriteString(Minutes,0,2);
        LCD_NOKIA_WriteString(Seconds,0,3);
        LCD_NOKIA_WriteString(var1,00,4);
        LCD_NOKIA_WriteString(var2,00,5);
        LCD_NOKIA_WriteString(var3,40,1);
        LCD_NOKIA_WriteString(var4,40,2);
        LCD_NOKIA_WriteString(var5,60,3);
        delay_ms(500);
        LCD_NOKIA_Clear();
    }
    return;
}
