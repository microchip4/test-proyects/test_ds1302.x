/*
 * Library:     DS1302
 *              Real time Clock
 * Author:      Brandon Ruiz Vasquez
 */

#include <xc.h>
#define _XTAL_FREQ 20000000
#include "DS1302_RTC.h"
#include "PCD8544.h"

/*
 * Function:        void DS1302_Init()
 * Description:     Initialize DS1302, Sets pins to Output 
 * Precondition:    None
 * Parameters:      None
 * Returns:         None
 */
void DS1302_Init(){
    DS1302_RST_TRIS = 0;
    DS1302_DAT_TRIS = 0;
    DS1302_CLK_TRIS = 0;
    DS1302_RST_PORT = 0;
    DS1302_DAT_PORT = 0;
    DS1302_CLK_PORT = 0;
}

/*
 * Function:        void DS1302_sendData(char data)
 * Description:     Low level functions, bitbangs RTC
 * Precondition:    DS1302_Init()
 * Parameters:      char data
 * Returns:         None
 */
void DS1302_sendData(char data){
    DS1302_DAT_TRIS = 0;
    char i;
    for (i = 0; i < 8; ++i) {
        DS1302_DAT_LATC = (data >> i);
        DS1302_CLK_PORT = 1;
		__delay_us(50);
		DS1302_CLK_PORT = 0;
        __delay_us(50);
    }
}

/*
 * Function:        DS1302_readData()
 * Description:     Low level functions, bitbangs RTC
 * Precondition:    DS1302_Init()
 * Parameters:      None
 * Returns:         char value
 */
char DS1302_readData(){
    DS1302_DAT_TRIS = 1;
    char value = 0;
    char i;
	for (i = 0; i < 8; ++i){
        char a = DS1302_DAT_PORT;
		value |= (a << i);
		DS1302_CLK_PORT = 1;
		__delay_us(50);
		DS1302_CLK_PORT = 0;
        __delay_us(50);
	}
	return value;
}

/*
 * Function:        char DS1302_DecToBCD(char Decimal)
 * Description:     Converts Decimal to BCD
 * Precondition:    None
 * Parameters:      char Decimal
 * Returns:         char
 */
char DS1302_DecToBCD(char Decimal){
    Decimal = Decimal % 100;        //Makes sure the value doesn't exeed 2 Digits
    char tens = (Decimal/10) << 4;  //Gets 10s and Shifts the to right 4 times
    return tens + (Decimal % 10);//Adds the 10s and Nums correctly
}

/*
 * Function:        char DS1302_BCDToDec(char BCD)
 * Description:     Converts BCD to Decimal
 * Precondition:    None
 * Parameters:      char BCD
 * Returns:         char
 */
char DS1302_BCDToDec(char BCD){
    return ((BCD >> 4) & 0b00001111)*10 + (BCD & 0b00001111);
}

/*
 * Function:        void DS1302_setSEC(char SEC)
 * Description:     Sets the seconds (00-60) in RTC
 * Precondition:    DS1302_Init()
 * Parameters:      char SEC
 * Returns:         None
 */
void DS1302_setSEC(char SEC){
    DS1302_sendData(0b10000000 + (DS1302_ADDRESS_SEC << 1) + 0);
    DS1302_sendData(DS1302_DecToBCD(SEC));
}

/*
 * Function:        char DS1302_getSEC()
 * Description:     Reads the Seconds (00-60) from RTC
 * Precondition:    None
 * Parameters:      None
 * Returns:         char
 */
char DS1302_getSEC(){
    DS1302_sendData(0b10000000 + (DS1302_ADDRESS_SEC << 1) + 1);
    int i = DS1302_readData();
    return DS1302_BCDToDec(i);
}

/*
 * Function:        void DS1302_setMIN(char MIN)
 * Description:     Writes desired Minutes  (00-60) to RTC
 * Precondition:    None
 * Parameters:      char MIN
 * Returns:         None
 */
void DS1302_setMIN(char MIN){
    DS1302_CLK_PORT = 0;
    DS1302_RST_PORT = 1;
    DS1302_DAT_LATC = 0;
    DS1302_sendData(0b10000000 + (DS1302_ADDRESS_MIN << 1) + 0);
    DS1302_sendData(DS1302_DecToBCD(MIN));
    DS1302_RST_PORT = 0;
}

/*
 * Function:        char DS1302_getMIN()
 * Description:     Reads Minutes (00-60) from RTC
 * Precondition:    None
 * Parameters:      None
 * Returns:         char
 */
char DS1302_getMIN(){
    DS1302_CLK_PORT = 0;
    DS1302_RST_PORT = 1;
    DS1302_DAT_LATC = 1;
    DS1302_sendData(0b10000000 + (DS1302_ADDRESS_MIN << 1) + 1);
    int i = DS1302_readData();
    DS1302_RST_PORT = 0;
    return DS1302_BCDToDec(i);
}

/*
 * Function:        void DS1302_setHR(char HR)
 * Description:     Writes desired hours (00-24) to RTC
 * Precondition:    None
 * Parameters:      char HR
 * Returns:         None
 */
void DS1302_setHR(char HR){
    DS1302_CLK_PORT = 0;
    DS1302_RST_PORT = 1;
    DS1302_DAT_LATC = 0;
    DS1302_sendData(0b10000000 + (DS1302_ADDRESS_HR << 1) + 0);
    DS1302_sendData(DS1302_DecToBCD(HR));
    DS1302_RST_PORT = 0;
}

/*
 * Function:        char DS1302_getHR()
 * Description:     Reads Year (00-24) from RTC
 * Precondition:    None
 * Parameters:      None
 * Returns:         char
 */
char DS1302_getHR(){
    DS1302_CLK_PORT = 0;
    DS1302_RST_PORT = 1;
    DS1302_DAT_LATC = 1;
    DS1302_sendData(0b10000000 + (DS1302_ADDRESS_HR << 1) + 1);
    int i = DS1302_readData();
    DS1302_RST_PORT = 0;
    return DS1302_BCDToDec(i);
}

/*
 * Function:        void DS1302_setDATE(char DATE)
 * Description:     Reads Date (01-31) from RTC
 * Precondition:    None
 * Parameters:      char DATE
 * Returns:         None
 */
void DS1302_setDATE(char DATE){
    DS1302_CLK_PORT = 0;
    DS1302_RST_PORT = 1;
    DS1302_DAT_LATC = 0;
    DS1302_sendData(0b10000000 + (DS1302_ADDRESS_DATE << 1) + 0);
    DS1302_sendData(DS1302_DecToBCD(DATE));
    DS1302_RST_PORT = 0;
}

/*
 * Function:        char DS1302_getDATE()
 * Description:     Writes Date (01-31) from RTC
 * Precondition:    None
 * Parameters:      None
 * Returns:         char
 */
char DS1302_getDATE(){
    DS1302_CLK_PORT = 0;
    DS1302_RST_PORT = 1;
    DS1302_DAT_LATC = 1;
    DS1302_sendData(0b10000000 + (DS1302_ADDRESS_DATE << 1) + 1);
    int i = DS1302_readData();
    DS1302_RST_PORT = 0;
    return DS1302_BCDToDec(i);
}

/*
 * Function:        void DS1302_setMONTH(char MONTH)
 * Description:     Writes Month (01-12) to RTC
 * Precondition:    None
 * Parameters:      char MONTH
 * Returns:         None
 */
void DS1302_setMONTH(char MONTH){
    DS1302_CLK_PORT = 0;
    DS1302_RST_PORT = 1;
    DS1302_DAT_LATC = 0;
    DS1302_sendData(0b10000000 + (DS1302_ADDRESS_MONTH << 1) + 0);
    DS1302_sendData(DS1302_DecToBCD(MONTH));
    DS1302_RST_PORT = 0;
}

/*
 * Function:        char DS1302_getMONTH()
 * Description:     Reads Month (01-12) from RTC
 * Precondition:    None
 * Parameters:      None
 * Returns:         char
 */
char DS1302_getMONTH(){
    DS1302_CLK_PORT = 0;
    DS1302_RST_PORT = 1;
    DS1302_DAT_LATC = 1;
    DS1302_sendData(0b10000000 + (DS1302_ADDRESS_MONTH << 1) + 1);
    int i = DS1302_readData();
    DS1302_RST_PORT = 0;
    return DS1302_BCDToDec(i);
}

/*
 * Function:        void DS1302_setDAY(char DAY)
 * Description:     Writes Day of Week (01-07) to RTC
 * Precondition:    None
 * Parameters:      char DAY
 * Returns:         None
 */
void DS1302_setDAY(char DAY){
    DS1302_CLK_PORT = 0;
    DS1302_RST_PORT = 1;
    DS1302_DAT_LATC = 0;
    DS1302_sendData(0b10000000 + (DS1302_ADDRESS_DAY << 1) + 0);
    DS1302_sendData(DS1302_DecToBCD(DAY));
    DS1302_RST_PORT = 0;
}

/*
 * Function:        char DS1302_getDAY()
 * Description:     Reads Day of Week (01-07) to RTC
 * Precondition:    None
 * Parameters:      None
 * Returns:         char
 */
char DS1302_getDAY(){
    DS1302_CLK_PORT = 0;
    DS1302_RST_PORT = 1;
    DS1302_DAT_LATC = 1;
    DS1302_sendData(0b10000000 + (DS1302_ADDRESS_DAY << 1) + 1);
    int i = DS1302_readData();
    DS1302_RST_PORT = 0;
    return DS1302_BCDToDec(i);
}

/*
 * Function:        void DS1302_setYEAR(char YEAR)
 * Description:     Reads Year (00-99)
 * Precondition:    None
 * Parameters:      char YEAR
 * Returns:         None
 */
void DS1302_setYEAR(char YEAR){
    DS1302_CLK_PORT = 0;
    DS1302_RST_PORT = 1;
    DS1302_DAT_LATC = 0;
    DS1302_sendData(0b10000000 + (DS1302_ADDRESS_YEAR << 1) + 0);
    DS1302_sendData(DS1302_DecToBCD(YEAR));
    DS1302_RST_PORT = 0;
}

/*
 * Function:        char DS1302_getYEAR()
 * Description:     Reads Year (00-99)
 * Precondition:    None
 * Parameters:      None
 * Returns:         char
 */
char DS1302_getYEAR(){
    DS1302_CLK_PORT = 0;
    DS1302_RST_PORT = 1;
    DS1302_sendData(0b10000000 + (DS1302_ADDRESS_YEAR << 1) + 1);
    int i = DS1302_readData();
    DS1302_RST_PORT = 0;
    return DS1302_BCDToDec(i);
}

/*
 * Function:        void DS1302_setCONTROL(char CONTROL)
 * Description:     Writes Control Register (bit 7 WP)
 * Precondition:    None
 * Parameters:      char CONTROL
 * Returns:         None
 */
void DS1302_setCONTROL(char CONTROL){
    DS1302_CLK_PORT = 0;
    DS1302_RST_PORT = 1;
    DS1302_DAT_LATC = 0;
    DS1302_sendData(0b10000000 + (DS1302_ADDRESS_CONTROL << 1) + 0);
    DS1302_sendData(CONTROL);
    DS1302_RST_PORT = 0;
}

/*
 * Function:        char DS1302_getCONTROL()
 * Description:     Reads Control Register (bit 7 WP)
 * Precondition:    None
 * Parameters:      None
 * Returns:         Control Register
 */
char DS1302_getCONTROL(){
    DS1302_CLK_PORT = 0;
    DS1302_RST_PORT = 1;
    DS1302_sendData(0b10000000 + (DS1302_ADDRESS_CONTROL << 1) + 1);
    char i = DS1302_readData();
    DS1302_RST_PORT = 0;
    return i;
}

/*
 * Function:        void DS1302_setTrCHARGE(char TrCHARGE)
 * Description:     Set TRICKLE CHARGER Configuration
 * Precondition:    None
 * Parameters:      char TrCHARGE
 * Returns:         None
 */
void DS1302_setTrCHARGE(char TrCHARGE){
    DS1302_CLK_PORT = 0;
    DS1302_RST_PORT = 1;
    DS1302_DAT_LATC = 0;
    DS1302_sendData(0b10000000 + (DS1302_ADDRESS_TR_CHARGE << 1) + 0);
    DS1302_sendData(DS1302_DecToBCD(TrCHARGE));
    DS1302_RST_PORT = 0;
}
/*
 * Function:        char DS1302_getTrCHARGE()
 * Description:     Reads TRICKLE CHARGER Configuration
 * Precondition:    None
 * Parameters:      None
 * Returns:         char TRICKLE CHARGER Configuration
 */
char DS1302_getTrCHARGE(){
    DS1302_CLK_PORT = 0;
    DS1302_RST_PORT = 1;
    DS1302_sendData(0b10000000 + (DS1302_ADDRESS_TR_CHARGE << 1) + 1);
    int i = DS1302_readData();
    DS1302_RST_PORT = 0;
    return DS1302_BCDToDec(i);
}

/*
 * Function:        void DS1302_ClockBurst(char RD_W)
 * Description:     Puts the RTC in ClockBurst Mode
 *                  It can RD/W all Time Param consecutively
 * Precondition:    None
 * Parameters:      char RD_W
 * Returns:         None
 */
void DS1302_ClockBurst(char RD_W){
    DS1302_CLK_PORT = 0;
    DS1302_RST_PORT = 1;
    DS1302_sendData(0b10000000 + (DS1302_ADDRESS_CLK_BURTS << 1) + RD_W);
}

/*
 * Function:        void DS1302_setRAM(char ADDRESS, char DATA)
 * Description:     Sets data in address from RTC RAM
 * Precondition:    None
 * Parameters:      char ADDRESS, char Data
 * Returns:         None
 */
void DS1302_setRAM(char ADDRESS, char DATA){
    DS1302_CLK_PORT = 0;
    DS1302_RST_PORT = 1;
    DS1302_sendData(0b10000000 + (ADDRESS << 1) + 0);   //RAM Address
    DS1302_sendData(DS1302_DecToBCD(DATA));             //Data on RAM
    DS1302_RST_PORT = 0;
}

/*
 * Function:        char DS1302_getRAM(char ADDRESS)
 * Description:     Gets data from address in RTC RAM
 * Precondition:    None
 * Parameters:      char ADDRESS
 * Returns:         char (Data from address)
 */
char DS1302_getRAM(char ADDRESS){
    DS1302_CLK_PORT = 0;
    DS1302_RST_PORT = 1;
    DS1302_DAT_LATC = 0;
    DS1302_sendData(0b10000000 + (ADDRESS << 1) + 1);
    char i = DS1302_readData();
    DS1302_RST_PORT = 0;
    return i;
}

/*
 * Function:        void DS1302_setTIME(...)
 * Description:     Sets time into RTC (data in char)
 * Precondition:    None
 * Parameters:      char YEAR, char DAYW, char MONTH, char DAYM, 
 *                  char HOURS, char MINUTES, char SECONDS
 */
void DS1302_setTIME(char YEAR, char DAYW, char MONTH, char DAYM, char HOURS, char MINUTES, char SECONDS){
    DS1302_RST_PORT = 1;
    DS1302_ClockBurst(0);
    DS1302_sendData(DS1302_DecToBCD(SECONDS));
    DS1302_sendData(DS1302_DecToBCD(MINUTES));
    DS1302_sendData(DS1302_DecToBCD(HOURS));
    DS1302_sendData(DS1302_DecToBCD(DAYM));
    DS1302_sendData(DS1302_DecToBCD(MONTH));
    DS1302_sendData(DS1302_DecToBCD(DAYW));
    DS1302_sendData(DS1302_DecToBCD(YEAR));
    DS1302_sendData(0);
    DS1302_RST_PORT = 0;
}

/* Function:        void DS1302_getTIME(char * buff)
 * Description:     Saves date and time in buff
 * Precondition:    None
 * Parameters:      buff contains the pointer where data will be
 *                  stored (must be size 8)
 * */
void DS1302_getTIME(char * buff){
    DS1302_CLK_PORT = 0;
    DS1302_RST_PORT = 1;
    DS1302_DAT_LATC = 0;
    DS1302_ClockBurst(1);
    char i;
    for (i = 0; i < 7; i++) {
        *buff++ = DS1302_BCDToDec(DS1302_readData());
    }
    DS1302_RST_PORT = 0;
}