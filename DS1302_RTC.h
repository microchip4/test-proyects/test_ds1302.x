/* 
 * File:   DS1302_RTC.h
 * Author: Brandon Ruiz Vasquez
 *
 * Created on 2020/05/20, 21:42
 */

#ifndef DS1302_RTC_H
#define	DS1302_RTC_H

#include <xc.h>


#ifdef	__cplusplus
extern "C" {
#endif

#define DS1302_ADDRESS_SEC          0x00
#define DS1302_ADDRESS_MIN          0x01
#define DS1302_ADDRESS_HR           0x02
#define DS1302_ADDRESS_DATE         0x03
#define DS1302_ADDRESS_MONTH        0x04
#define DS1302_ADDRESS_DAY          0x05
#define DS1302_ADDRESS_YEAR         0x06
#define DS1302_ADDRESS_CONTROL      0x07
#define DS1302_ADDRESS_TR_CHARGE    0x08
#define DS1302_ADDRESS_CLK_BURTS    0x1F
    
/*    
    enum DS1302_ADDRESS{
        SEC    = 0x00,
        MIN    = 0x01,
        HR     = 0x02,
        DATE   = 0x03,
        MONTH  = 0x04,
        DAY    = 0x05,
        YEAR   = 0x06,
        CONTROL    = 0x07,
        TR_CHARG   = 0x08,
        CLK_BURST  = 0x1F
    };
*/
/*
struct DS1302_TIME{
    char YEAR   : 7;
    char MONTH  : 4;
    char DAYM   : 5;
    char DAYW   : 3;
    char HOUR   : 5;
    char MIN    : 6;
    char SEC    : 6;
};
 * */

#define DS1302_RST_TRIS     TRISBbits.TRISB0
#define DS1302_RST_PORT     LATBbits.LATB0
#define DS1302_DAT_TRIS     TRISBbits.TRISB1
#define DS1302_DAT_PORT     PORTBbits.RB1
#define DS1302_DAT_LATC     LATBbits.LATB1
#define DS1302_CLK_TRIS     TRISBbits.TRISB2
#define DS1302_CLK_PORT     LATBbits.LATB2

void DS1302_Init();
    
void DS1302_sendData(char data);
char DS1302_readData();

char DS1302_DecToBCD(char Decimal);
char DS1302_BCDToDec(char BCD);

void DS1302_halt(char enable);
void DS1302_setSEC(char SEC);
char DS1302_getSEC();
void DS1302_setMIN(char MIN);
char DS1302_getMIN();
void DS1302_setHR(char HR);
char DS1302_getHR();
void DS1302_setDATE(char DATE);
char DS1302_getDATE();
void DS1302_setMONTH(char MONTH);
char DS1302_getMONTH();
void DS1302_setDAY(char DAY);
char DS1302_getDAY();
void DS1302_setYEAR(char YEAR);
char DS1302_getYEAR();
void DS1302_setCONTROL(char CONTROL);
char DS1302_getCONTROL();
void DS1302_setTrCHARGE(char TrCHARGE);
char DS1302_getTrCHARGE();
void DS1302_ClockBurst(char RD_W);
void DS1302_setRAM(char ADDRESS, char DATA);
char DS1302_getRAM(char ADDRESS);
void DS1302_setTIME(char YEAR, char DAYW, char MONTH, char DAYM, char HOURS, char MINUTES, char SECONDS);
void DS1302_getTIME(char *buff);

#ifdef	__cplusplus
}
#endif

#endif	/* DS1302_RTC_H */

